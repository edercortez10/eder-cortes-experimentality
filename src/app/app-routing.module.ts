import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./paginas/home/home.component";
import {ListadoProductosComponent} from "./paginas/listado-productos/listado-productos.component";
import { CarritoComponent } from './paginas/carrito/carrito.component';

const routes: Routes = [

  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'listado-de-productos/:categoria',
    component: ListadoProductosComponent
  },
  {
    path: 'carrito-de-compra',
    component: CarritoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash : true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
