import { Component, OnInit } from '@angular/core';
import {SeguridadComponent} from "../seguridad/seguridad.component";
import {CarritoGlobalService} from "../../servicios/global/carrito-global.service";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  carritoAnterior: any;
  valorCantidad: number = 0;
  colorOpcionMenu1: string = "#FFFFFF";
  colorOpcionMenu2: string = "#FFFFFF";
  colorOpcionMenu3: string = "#FFFFFF";
  colorOpcionMenu4: string = "#FFFFFF";
  colorOpcionMenu5: string = "#FFFFFF";
  colorOpcionMenu6: string = "#FFFFFF";
  constructor(private variablesGl: CarritoGlobalService, public dialog: MatDialog, private  ruta: Router) { }

  ngOnInit(): void {
    this.llamarDatoLocales();
  }

  /**
   * Abre el modal del formulario para iniciar sesión
   * @param tipo
   */
  continuarConLaCompra(tipo: number) {

    const dialogRef = this.dialog.open(SeguridadComponent, {
      width: '500px',
      height: '420px',
      data: {
        id: tipo,
        mover: 0
      }
    });

  }

  /**
   * Metodo que esta a la escucha validandos e el carrioto de compra sufre algun cambio
   */

  llamarDatoLocales() {

    this.variablesGl.currentMessage.subscribe(response => {
      this.carritoAnterior = response;

      this.cantidadPedidoInicial();
    });
  }

  /**
   * Metodo encargado de traer la cantidad de items que tiene el carrito y pintarlos en pantalla
   */
  cantidadPedidoInicial() {

    // @ts-ignore
    const valorTotalLista = JSON.parse(localStorage.getItem('pruebaTecnica'));
    if (valorTotalLista) {
      this.valorCantidad = valorTotalLista.reduce((item1: any, item2: any) => {
        return item1 + parseInt(item2.producto_cantidad);

      }, 0);
    }
    console.log(this.valorCantidad);
    return  this.valorCantidad;

  }

  /**
   * Metodo que me lleva al listado de producto cuando tandto web como movil
   * @param ruta
   */
  listadoProductoTotal(ruta: any) {

    this.ruta.navigate(['/listado-de-productos/'+ruta]);

    // @ts-ignore
    document.getElementById('validationDefaultUsername').blur();

  }

  /**
   * Abrir modal modo celular
   */

   openNav() {
    // @ts-ignore
     document.getElementById("mySidenav").style.width = "310px";
  }

  /**
   * Cerrar modal modo celular
   */
  closeNav() {
    // @ts-ignore
    document.getElementById("mySidenav").style.width = "0";
  }

  /**
   * Metodo apra amrcar la sesión seleccionada
   * @param sesion
   */
  verSesionConsultada(sesion: number) {

    this.colorOpcionMenu1 = "#FFFFFF";
    this.colorOpcionMenu2 = "#FFFFFF";
    this.colorOpcionMenu3 = "#FFFFFF";
    this.colorOpcionMenu4 = "#FFFFFF";
    this.colorOpcionMenu5 = "#FFFFFF";
    this.colorOpcionMenu6 = "#FFFFFF";

    if (sesion === 1) {
      this.colorOpcionMenu1 = '#8A2BE2FF';
    }
    if (sesion === 2) {
      this.colorOpcionMenu2 = '#8A2BE2FF';
    }
    if (sesion === 3) {
      this.colorOpcionMenu3 = '#8A2BE2FF';
    }
    if (sesion === 4) {
      this.colorOpcionMenu4 = '#8A2BE2FF';
    }
    if (sesion === 5) {
      this.colorOpcionMenu5 = '#8A2BE2FF';
    }
    if (sesion === 6) {
      this.colorOpcionMenu6 = '#8A2BE2FF';
    }

    if (sesion === 0) {
      this.colorOpcionMenu1 = "#FFFFFF";
      this.colorOpcionMenu2 = "#FFFFFF";
      this.colorOpcionMenu3 = "#FFFFFF";
      this.colorOpcionMenu4 = "#FFFFFF";
      this.colorOpcionMenu5 = "#FFFFFF";
      this.colorOpcionMenu6 = "#FFFFFF";
    }
    
  }

  abrirCarritoCompra() {

    this. verSesionConsultada(0);
    this.ruta.navigate(['/carrito-de-compra']);
  }
}
