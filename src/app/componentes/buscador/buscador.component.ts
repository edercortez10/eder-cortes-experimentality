import {Component, OnInit} from '@angular/core';
import {CrudService} from "../../servicios/crud.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.scss']
})
export class BuscadorComponent implements OnInit {

  dataBuscador: any;
  recorerBusqueda = [];
  producto: any;
  url: any;
  bolsa = false;
  mostrarResultados: boolean;

  constructor(private crud: CrudService, private ruta: Router) {
    this.mostrarResultados = false;
  }

  ngOnInit(): void {

    this.dataBuscador = null;

  }

  // @ts-ignore
  buscadorProductos(e) {
  }

  ocultarInformacionBuscador() {

    if (this.dataBuscador) {
      this.mostrarResultados = true;
      // @ts-ignore
      document.getElementById('validationDefaultUsername').focus();
      this.showSearchResults();
    }


  }

  perderFocusOcultarResulBusqueda() {
    // @ts-ignore
    document.getElementById('validationDefaultUsername').blur();
    this.mostrarResultados = false
  }

  /**
   * Metodo encargado de traer todos los producto cuandos e hace una busqueda
   */
  showSearchResults() {
    if (this.dataBuscador && this.dataBuscador.length > 2) {

      this.crud.getBuscarProductos(this.dataBuscador.toUpperCase()).then(respuesta => {
        if (respuesta['results'].length > 0) {
          this.recorerBusqueda = respuesta['results'];
        } else {
          this.recorerBusqueda = [];
        }
      }).catch(error => {
      });
    } else {
      this.recorerBusqueda = [];
    }

  }

  /**
   * Metodo encargado de navegar al listado de producto cuando se haga una busqueda y se seleccione un item el cual
   * resive como parametro la categoria de la busqueda selecciona
   * @param ruta
   */
  listadoProductoTotal(ruta: any) {
    this.ruta.navigate(['/listado-de-productos/' + ruta]);
    // @ts-ignore
    document.getElementById('validationDefaultUsername').blur();

    this.mostrarResultados = false


  }
}
