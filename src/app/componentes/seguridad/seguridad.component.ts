import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-seguridad',
  templateUrl: './seguridad.component.html',
  styleUrls: ['./seguridad.component.scss']
})
export class SeguridadComponent implements OnInit {
  condicion: number = 0;
  registroUsuario: any;
  ALERT_TYPE: string = '';
  ALERT_MESSAGE: string = '';
  ALERT_SHOW: string = '';
  showPassword: any;
  confirmarClave: any;
  enrutar: any;
  loginUsuario: any;
  constructor(public dialogRef: MatDialogRef<SeguridadComponent>,
              @Inject(MAT_DIALOG_DATA) public infor: any,) {

    console.log(infor)
    this.condicion = infor.id;
  }

  ngOnInit(): void {
  }


  cambiarFormulario(valor: number) {
    this.condicion = valor;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  signInWithGoogle(): void {}

  signInWithFB(): void {
  }

  autenticarse() {}

  crearUsuario() {}
}
