import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
@Component({
  selector: 'app-flecha-arriba',
  templateUrl: './flecha-arriba.component.html',
  styleUrls: ['./flecha-arriba.component.scss']
})
export class FlechaArribaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $(document).ready(function(){

      $('.ir-arriba').click(function(){
        $('body, html').animate({
          scrollTop: '0px'
        }, 300);
      });

      $(window).scroll(function(){
        // @ts-ignore
        if( $(this).scrollTop() > 0 ){
          $('.ir-arriba').slideDown(300);
        } else {
          $('.ir-arriba').slideUp(300);
        }
      });

    });
  }


}
