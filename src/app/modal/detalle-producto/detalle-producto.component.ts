import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AlertasService} from "../../totificaciones/alertas/alertas.service";
import {CarritoGlobalService} from "../../servicios/global/carrito-global.service";

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.scss']
})
export class DetalleProductoComponent implements OnInit {
  selectCantidad: any;
  carritoAnterior = [];
  addProductoCarrito = [];
  almacenProducto: any;
  constructor(private alerta: AlertasService,
              private variablesGl: CarritoGlobalService,
              public dialogRef: MatDialogRef<DetalleProductoComponent>,
              @Inject(MAT_DIALOG_DATA) public infor: any, ) {

    console.log('asdasd', infor);
  }

  ngOnInit(): void {
    this.selectCantidad = 1
  }

  /**
   * Metodo encargado de cerrar el modal del detalle del Producto
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * Este metodo es para sumar o disminuir la cantidad antes de agregarlo al carrito siempre por defecto sera "1"
   * @param dato
   */
  manipularCantidad(dato: boolean) {

      if (dato) {
        this.selectCantidad = this.selectCantidad +1;

      } else {
        if ( this.selectCantidad > 1) {
          this.selectCantidad = this.selectCantidad -1;
        } else {
        }

      }

  }

  /**
   * Metodo encargado de agregar el producto al carrito de compra
   */
  agregarProductoAlCarrito(){


    let descuento = 0;



    if (!this.selectCantidad) {
      const mensaje = 'La cantidad es obligatoria para continuar el proceso';

      return;
    }


    console.log(this.infor['data'])
    this.almacenProducto =
      {
        producto_descuento: this.valorDescuento(this.infor['data']['original_price'], this.infor['data']['price']),
        producto_cantidad: this.selectCantidad,
        producto_imagen: this.infor['data']['thumbnail'],
        producto_valor: this.infor['data']['original_price'],
        producto_valortotal: this.infor['data']['price'],
        producto_nombre: this.infor['data']['title'],
        producto_descripcion: this.infor['data']['producto_descripcion'],

      }

    // @ts-ignore
    this.carritoAnterior = JSON.parse(localStorage.getItem('pruebaTecnica'));
    // @ts-ignore
    this.addProductoCarrito.push(this.almacenProducto);

    if (this.addProductoCarrito) {
      if (this.carritoAnterior) {
      } else {
        this.carritoAnterior = [];
      }

      // @ts-ignore
      this.carritoAnterior.push(this.almacenProducto);
      localStorage.setItem('pruebaTecnica', JSON.stringify(this.carritoAnterior));

      this.selectCantidad = 1;
      this.variablesGl.changeMessage();
      this.alerta.showToasterFull('Producto agregado al carrido :) ');
      this.onNoClick();
    }

    this.addProductoCarrito = [];

  }

  /**
   * Este meto es para calcular le porcentaje de descuento del producto apsando como aprametro el precio original y el nuevo
   * @param origina
   * @param final
   */
  valorDescuento(origina: any, final: any) {

    let divicion;
    let sigiente;
    let sigientepaso;
    divicion = final/origina;
    sigiente = divicion*100;
    sigientepaso =  sigiente - 100;

    return sigientepaso;

    console.log(origina+',',  final)

  }
}
