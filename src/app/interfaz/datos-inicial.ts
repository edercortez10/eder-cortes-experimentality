export  interface DatosInicial {
  site_id: string,
  paging: object,
  results: []
  secondary_results: []
  related_results: []
  sort: object
  available_sorts: []
  filters: []
  available_filters: []
}
