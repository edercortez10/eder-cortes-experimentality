export interface Productos {

available_quantity: 1
catalog_product_id: number
category_id: string
condition:string
currency_id: string;
domain_id: string
id: string
listing_type_id: string
official_store_id: null
order_backend: number;
original_price: number;
permalink: string;
price: number;
sale_price: string;
sold_quantity: number;
stop_time: string;
thumbnail: string;
thumbnail_id: string;
title: string;

};


