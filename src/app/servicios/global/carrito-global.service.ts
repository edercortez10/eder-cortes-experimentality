import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarritoGlobalService {

  private data = new BehaviorSubject(null);
  currentMessage = this.data.asObservable();
  //data: any;

  changeMessage() {

    this.consultarDatosLocales();
  }

  constructor() {

    this.consultarDatosLocales();

  }

  consultarDatosLocales() {
    // @ts-ignore
    this.data.next(JSON.parse(localStorage.getItem('pruebaTecnica'))) ;
  }
}
