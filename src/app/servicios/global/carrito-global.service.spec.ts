import { TestBed } from '@angular/core/testing';

import { CarritoGlobalService } from './carrito-global.service';

describe('CarritoGlobalService', () => {
  let service: CarritoGlobalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarritoGlobalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
