import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Productos} from "../interfaz/productos";
import {DatosInicial} from "../interfaz/datos-inicial";
declare const require: any;

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private httpC: HttpClient) {

  }


  geListadoBusqueda(data: string): Promise<DatosInicial>{

    return  this.httpC.get<DatosInicial>(`https://api.mercadolibre.com/sites/MCO/search?category=${data}`).toPromise();

  }


  getproductos(data: string): Promise<DatosInicial>{


    return  this.httpC.get<DatosInicial>(`https://api.mercadolibre.com/sites/MCO/search?category=MCO1430`).toPromise();


    /*return new Promise(resolve => {
      this.httpC.get<Productos>(`https://api.mercadolibre.com/sites/MCO/search?category=MCO1430`).subscribe((resul: Productos)  => {
        resolve(resul);
      }, error => {
        console.log(error);
      });
    });*/

  }

  getcategorias(ruta: string){
    return new Promise(resolve => {
      this.httpC.get(`https://api.mercadolibre.com/categories/${ruta}` ).subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
      });
    });

  }

  getBuscarProductos(data: any){

    return  this.httpC.get<DatosInicial>(`https://api.mercadolibre.com/sites/MCO/search?category=MCO1430&q=${data}`).toPromise();
   /* return new Promise(resolve => {
      this.httpC.get(`https://api.mercadolibre.com/sites/MCO/search?category=MCO1430&q=${data}` ).subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
      });
    });*/

  }

}
