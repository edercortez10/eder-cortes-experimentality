import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './componentes/header/header.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { HomeComponent } from './paginas/home/home.component';
import { CarritoComponent } from './paginas/carrito/carrito.component';
import { DetalleComponent } from './paginas/detalle/detalle.component';
import { SeguridadComponent } from './componentes/seguridad/seguridad.component';
import { BuscadorComponent } from './componentes/buscador/buscador.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import { MatCarouselModule } from '@ngmodule/material-carousel';
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule} from "@angular/forms";
import { ListadoProductosComponent } from './paginas/listado-productos/listado-productos.component';
import {MatCardModule} from '@angular/material/card';
import { DetalleProductoComponent } from './modal/detalle-producto/detalle-producto.component';
import {MatDialogModule} from "@angular/material/dialog";
import { FlechaArribaComponent } from './componentes/flecha-arriba/flecha-arriba.component';
import { CarruselHomeComponent } from './carrusel/carrusel-home/carrusel-home.component';
import {ToastrModule} from 'ngx-toastr';
import {HttpClientModule} from '@angular/common/http';
import {MatBadgeModule} from "@angular/material/badge";
import {MatExpansionModule} from '@angular/material/expansion';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    CarritoComponent,
    DetalleComponent,
    SeguridadComponent,
    BuscadorComponent,
    ListadoProductosComponent,
    DetalleProductoComponent,
    FlechaArribaComponent,
    CarruselHomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCarouselModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
    MatDialogModule,
    HttpClientModule,
    MatBadgeModule,
    MatExpansionModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      /*positionClass: 'toast-bottom-right',
      //positionClass: 'toast-bottom-left',*/
      preventDuplicates: true,
    }),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
