import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {CrudService} from "../../servicios/crud.service";
import {DetalleProductoComponent} from "../../modal/detalle-producto/detalle-producto.component";
import {DatosInicial} from "../../interfaz/datos-inicial";
import {Productos} from "../../interfaz/productos";

@Component({
  selector: 'app-carrusel-home',
  templateUrl: './carrusel-home.component.html',
  styleUrls: ['./carrusel-home.component.scss']
})
export class CarruselHomeComponent implements OnInit {
  contadorIndices = 0;
  private longitudTexto: any;
  masVendidos: Array<Productos>;
  constructor(private crud: CrudService, public dialog: MatDialog) {

    this.masVendidos = [];
  }


  ngOnInit(): void {

    this.productosRelacionados();
  }


  productosRelacionados(){

    this.masVendidos = [];

    this.crud.getproductos('MCO1430').then((respeusta: DatosInicial) => {

      if (!respeusta) {return;}

      const resultados: Array<Productos> = respeusta.results;
      const cantidadItems = resultados.length;
      const indicesAleatoreo: Array<number> = [];

      for (let i = 0; i < 20; i++) {
        let randomIndex = null;

        while (randomIndex === null || indicesAleatoreo.includes(randomIndex)) {
          randomIndex = Math.round(Math.random() * cantidadItems);
        }
        indicesAleatoreo.push(randomIndex);
      }

      indicesAleatoreo.forEach(index => {
          this.masVendidos.push(resultados[index]);
      });
    });
  }

  pasarDerecha(){
    this.contadorIndices++;
    let element = document.getElementById("inde-detalle_" + this.contadorIndices);
    let contenedor = document.getElementById('contenedor-principal');

    // @ts-ignore
    contenedor.scrollTo({
      top: 0,    // @ts-ignore
      left: element.offsetLeft,
      behavior: 'smooth'
    });

  }
  pasarIzquierda(){
    this.contadorIndices--;
    let element = document.getElementById("inde-detalle_" + this.contadorIndices);
    let contenedor = document.getElementById('contenedor-principal');


    // @ts-ignore
    contenedor.scrollTo({
      top: 0,// @ts-ignore
      left: element.offsetLeft,
      behavior: 'smooth'
    });


  }

  deshabilitarDerecha() {
    return this.masVendidos && this.contadorIndices === (this.masVendidos.length - 4)
  }
  deshabilitarIzquierda() {
    return this.contadorIndices === 0;
  }


  sacarPorcentaje(valor: any, porcentaje: any) {
    let valorPorcentaje = 0;
    let valorFinal = 0;
    if (porcentaje > 0) {
      valorPorcentaje = valor * porcentaje / 100;
      valorFinal = valor - valorPorcentaje;
    } else {
      valorFinal = valor;
    }
    return valorFinal;
  }

  mostrarNombreProducto(texto: any) {

    this.longitudTexto = texto.length;
    return texto;
  }
  valorDescuento(origina: any, final: any) {

    let divicion;
    let sigiente;
    let sigientepaso;
    divicion = final/origina;
    sigiente = divicion*100;
    sigientepaso =  sigiente - 100;
    return sigientepaso;

  }


  cargarModalUsuarioLogin(data: object) {
    const dialogRef = this.dialog.open(DetalleProductoComponent, {
      width: '800px',
      height: '400px',
      data: {
        data: data,
        mover: 0
      }
    });
  }



}
