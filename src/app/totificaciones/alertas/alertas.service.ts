import { Injectable } from '@angular/core';
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class AlertasService {


  constructor(private toastr: ToastrService) { }


  // tslint:disable-next-line:typedef
  showToasterFull(mensaje: string) {
    this.toastr.success(mensaje);
  }

  // tslint:disable-next-line:typedef
  showToasterError(mensaje: string) {
    this.toastr.error(mensaje);
  }

  // tslint:disable-next-line:typedef
  showToasterUpdate(mensaje: string) {
    this.toastr.success(mensaje);
  }

  // tslint:disable-next-line:typedef
  showToasterWarning(mensaje: string) {
    this.toastr.warning(mensaje);
  }
}
