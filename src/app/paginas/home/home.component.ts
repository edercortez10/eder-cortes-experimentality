import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  slides = [
    {
      img: 'assets/img/slider/banner-principal-desktop.png',
    },
    {
      img: 'assets/img/slider/banner-principal-desktop@2x.png',
    },

  ];

  constructor() { }

  ngOnInit(): void {
  }

}
