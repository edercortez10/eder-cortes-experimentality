import { Component, OnInit } from '@angular/core';
import {CarritoGlobalService} from "../../servicios/global/carrito-global.service";
import {DetalleProductoComponent} from "../../modal/detalle-producto/detalle-producto.component";
import {MatDialog} from "@angular/material/dialog";

import {SeguridadComponent} from '../../componentes/seguridad/seguridad.component'
import {AlertasService} from "../../totificaciones/alertas/alertas.service";

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {

  datosStorage = [];
  carrito: any;
  cantidadCarrito = 0;
  valorTotal: any;
  valorCantidad: number = 0;
  carritoNuevo = [];

  constructor(private variablesGl: CarritoGlobalService, public dialog: MatDialog, private alerta: AlertasService) { }

  ngOnInit(): void {
    this.llamarDatoLocales();
    this.miCarritoCompraContador();

  }


  /**
   * Metodo encargado de eliminar item del carrito de compra
   */

  eliminarProductodelCarrito(data: any, index: any) {

    this.carrito = localStorage.getItem('pruebaTecnica');
    let dataCarrito = JSON.parse(this.carrito);
    let i = dataCarrito.indexOf(data);

    dataCarrito.splice(index, 1);

    localStorage.setItem('pruebaTecnica', JSON.stringify(dataCarrito));
    this.llamarDatoLocales();
    this.variablesGl.changeMessage();
    this.alerta.showToasterFull('Producto removido del carrito de compra exitosamente');

  }

  /**
   * Metodo encargado de escuchar cada ves que el carrito de compra sufra un cambio, se agregue se elimine, y se sume un item mas
   */
  llamarDatoLocales() {

    this.variablesGl.currentMessage.subscribe(response => {
      // @ts-ignore
      this.datosStorage = response;
      this.valorTotalPedido();
      this.cantidadPedidoInicial()
      this.miCarritoCompraContador();
    });
  }

  /**
   * Metodo encargado de mostrar el valor total a pagar
   */
  valorTotalPedido() {

    // @ts-ignore
    const valorTotalLista = JSON.parse(localStorage.getItem('pruebaTecnica'));
    if (valorTotalLista) {
      // @ts-ignore
      this.valorTotal = valorTotalLista.reduce((item1, item2) => {
        return item1 + (item2.producto_cantidad * item2.producto_valortotal);
      }, 0);
    }
    return this.valorTotal;

  }


  /**
   * Metodo encargado de ver la cantidad de Items agregado al carrito
   */
  cantidadPedidoInicial() {

    // @ts-ignore
    const valorTotalLista = JSON.parse(localStorage.getItem('pruebaTecnica'));
    if (valorTotalLista) {
      // @ts-ignore
      this.valorCantidad = valorTotalLista.reduce((item1, item2) => {
        return item1 + parseInt(item2.producto_cantidad);
      }, 0);
    }
    return  this.valorCantidad;

  }

  miCarritoCompraContador() {

    if (this.datosStorage) {
      this.datosStorage.forEach(respuesta => {
        this.cantidadCarrito += 1;
      });
    }

  }

  /**
   * Metodo encargado de sumar o disminuir la cantida de items  a un producto
   * @param data
   * @param indice
   * @param proceso
   */
  aumentarDisminuir(data: any, indice: any, proceso: any) {
    console.log(data, indice, proceso);
    // @ts-ignore
    this.carritoNuevo = JSON.parse(localStorage.getItem('pruebaTecnica'));

    if (proceso) {
      // @ts-ignore
      this.carritoNuevo[indice].producto_cantidad++;
      console.log(data, indice, proceso);
    } else {
      // @ts-ignore
      if (this.carritoNuevo[indice].producto_cantidad > 1) {
        // @ts-ignore
        this.carritoNuevo[indice].producto_cantidad--;
      } else {
        let datos = 'Articulo agregado a la canasta no puede ser menor a 1 unidad';
        //this.alertaS.showToasterWarning(datos);
      }
      console.log(data, indice, proceso);
    }

    localStorage.setItem('pruebaTecnica', JSON.stringify(this.carritoNuevo));
    this.variablesGl.changeMessage();

  }


  /**
   * Metodo encargadod e mostrar el modal de inicio de sesión para realizar la compra
   * @param tipo
   */
  continuarConLaCompra(tipo: number) {

    const dialogRef = this.dialog.open(SeguridadComponent, {
      width: '500px',
      height: '420px',
      data: {
        id: tipo,
        mover: 0
      }
    });
  }
}
