import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DetalleProductoComponent} from "../../modal/detalle-producto/detalle-producto.component";
import {CrudService} from "../../servicios/crud.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-listado-productos',
  templateUrl: './listado-productos.component.html',
  styleUrls: ['./listado-productos.component.scss']
})
export class ListadoProductosComponent implements OnInit {
  listadoProductos = [];
  parametro: any;
  constructor(
    public dialog: MatDialog,
    private crud: CrudService,
    private activatedRoute: ActivatedRoute,
    ) {


    this.activatedRoute.params.subscribe(value => {
      this.parametro = value.categoria;
      this.listarProductosApi();
    });

  }

  ngOnInit(): void {
    this.listarProductosApi()
  }

  /**
   * Cargar los productos de la api de Mercado libre
   */

  listarProductosApi() {
    this.crud.geListadoBusqueda(this.parametro).then(respeusta => {

      // @ts-ignore
      this.listadoProductos = respeusta.results;
    });
  }

  /**
   * Cargar modal del detalle de productos para seleccioanr la cantidad
   * @param data
   */
  cargarModalUsuarioLogin(data: number) {
    const dialogRef = this.dialog.open(DetalleProductoComponent, {
      width: '800px',
      height: '400px',
      data: {
        data: data,
        mover: 0
      }
    });
  }

  /**
   * Este metodo simplemente para ver el valor de descuento de cada roducto si lo tiene
   * @param origina
   * @param final
   */
  valorDescuento(origina: any, final: any) {

    let divicion;
    let sigiente;
    let sigientepaso;
    divicion = final/origina;
    sigiente = divicion*100;
    sigientepaso =  sigiente - 100;
    return sigientepaso;
  }

}
